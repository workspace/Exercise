package com.android.app;

import java.util.List;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.Utils.ImageLoader;
import com.android.Utils.HttpUtils;
import com.android.entity.News;
import com.android.entity.NewsItem;

public class MainActivity extends Activity implements OnClickListener {
	private static final String SERVER_URL = "http://thoughtworks-ios.herokuapp.com/facts.json";
	private Button refreshBtn;
	private ListView listView;
	private TextView tiltView;
	private ProgressBar progressBar;
	private List<NewsItem> listData;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		initUIControls();
		startLoading();
	}
	
	private void initUIControls(){
		listView = (ListView) this.findViewById(R.id.listview);
		tiltView = (TextView) this.findViewById(R.id.title);
		progressBar = (ProgressBar) this.findViewById(R.id.progressBar);
		refreshBtn = (Button) this.findViewById(R.id.refreshbtn);
		refreshBtn.setOnClickListener(this);
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.refreshbtn:
			startLoading();
			break;
		default:
			break;
		}

	}

	private void startLoading(){
		progressBar.setVisibility(View.VISIBLE);
		new NewsLoadTask().execute(SERVER_URL);
	}
	
	private class NewsAdpter extends BaseAdapter {
		@Override
		public int getCount() {
			return listData.size();
		}

		@Override
		public Object getItem(int position) {
			return listData.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			NewsItem item = listData.get(position);
			ViewHolder holder = null;
			if (convertView == null) {
				holder = new ViewHolder();
				convertView = getLayoutInflater().inflate(R.layout.activity_main_item, null);
				holder.titleView = (TextView) convertView.findViewById(R.id.newsItemTitle);
				holder.descriptionView = (TextView) convertView.findViewById(R.id.newsDescription);
				holder.ivImage = (ImageView) convertView.findViewById(R.id.ivImageUrl);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			
			//if imageUrl is not empty, then show the imageView and start to load the image.
			//Otherwise, hide the image view.
			holder.ivImage.setVisibility(View.GONE);
			String imageUrl = item.getImageUrl();
	        if (imageUrl != null && !imageUrl.isEmpty()) {
				new ImageLoader().loadBitmap(imageUrl, holder.ivImage);
				//holder.ivImage.setVisibility(View.VISIBLE);
			}
	        
	        String title = item.getTitle();
	        String description = item.getDescription();
			holder.titleView.setText(title.isEmpty() ? "" : title);
			holder.descriptionView.setText(description.isEmpty() ? "" : description);
			
			//if both imageUrl and description are empty, then hide these two components.
			if((imageUrl == null || imageUrl.isEmpty())
					&& description.isEmpty()){
				LinearLayout detailContainer = (LinearLayout) convertView.findViewById(R.id.detailInfo);
				detailContainer.setVisibility(View.GONE);
			}
			return convertView;
		}
	}

	//view holder to cache the UI controls.
	private class ViewHolder {
		private TextView titleView;
		private TextView descriptionView;
		private ImageView ivImage;
	}
	
	private class NewsLoadTask extends AsyncTask<String, Void, News> {

		@Override
		protected News doInBackground(String... params) {
			String data = HttpUtils.getJSONData(params[0]);
			return News.fromJsonToObj(data);
		}

		@Override
		protected void onPostExecute(News result) {
			if (result == null){
				progressBar.setVisibility(View.GONE);
				return;
			}
			listData = result.getNewsList();
			listView.setAdapter(new NewsAdpter());
			tiltView.setText(result.getTitle());
			progressBar.setVisibility(View.GONE);
		}

	}
}
